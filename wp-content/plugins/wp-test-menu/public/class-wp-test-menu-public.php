<?php

/**
 * The webiste-specific functionality of the plugin.
 * @package	WP_Test_Menu
 * @author	Sreejith C J	
 */
class WP_Test_Menu_Public{

	/**
	 * The ID of this plugin.
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 * @param      string    $plugin_name  The name of this plugin.
	 * @param      string    $version       The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}
    
    /**
     * Prepend the selected list item to each blog post
     */
	public function prepend_to_content( $content )
	{
         $args = array(
            'headers'     => array(
              'Token' => WP_TEST_MENU_TOKEN,
            ),
          ); 

         $request_uri = WP_TEST_MENU_API_URL.'/items/current';
         $request = wp_remote_get( $request_uri, $args  );

         if( is_wp_error( $request ) || '200' != wp_remote_retrieve_response_code( $request ) )
         return;
         
         $current_list_item_collection = json_decode( wp_remote_retrieve_body( $request ) );
		 $current_list_item = $current_list_item_collection[0][1];
		 $prependText = '<div>'.$current_list_item.'</div>';
		return  $prependText . $content;
	}

    /**
     * Prepend the selected list item to all blog post excerpts
     */
	public function prepend_to_excerpt($excerpt) {
		$args = array(
            'headers'     => array(
              'Token' => WP_TEST_MENU_TOKEN,
            ),
          ); 

         $request_uri = WP_TEST_MENU_API_URL.'/items/current';
         $request = wp_remote_get( $request_uri, $args  );

         if( is_wp_error( $request ) || '200' != wp_remote_retrieve_response_code( $request ) )
         return;
         
         $current_list_item_collection = json_decode( wp_remote_retrieve_body( $request ) );
		 $current_list_item = $current_list_item_collection[0][1];
		 $excerpt = str_replace($current_list_item,'',$excerpt);
		 $prependText = '<div>'.$current_list_item.'</div>';
		return  $prependText . $excerpt;
	}
}
