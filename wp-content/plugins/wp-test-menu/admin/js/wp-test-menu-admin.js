jQuery(document).ready(function($){
	$('#test_menu_form').submit(function()	{
		data ={
			action: 'test_menu_form',
			selected_item: $('#wp_test_menu').val()
		}
		$.post(ajaxurl, data,function(response){
			jQuery('#wp_menu_settings_message').prepend('<div class="notice"><p>'+response+'</p></div>');
		});
		
		return false;
	});
	$('#wp_test_menu').change(function() {
		$('#wp_menu_settings_message').html('');
	});
});