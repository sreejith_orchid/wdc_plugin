<?php

/**
 * The admin-specific functionality of the plugin.
 * @package	WP_Test_Menu
 * @author	Sreejith C J	
 */
?>
  <div class="wrap">
    <h1 class="wp-heading-inline">WP Test Menu Settings</h1>
    <h2 class="screen-reader-text">WP Test Menu Settings</h2>
      <div class="tablenav">
        <div id="wp_menu_settings_message"></div>
        <form action="" method="POST" id="test_menu_form" >			
            <label for="wp-test-menu" class="screen-reader-text">Select WP Test Menu item</label>
            <select name="wp-test-menu" id="wp_test_menu">
            <?php 
            
            $args = array(
              'headers'     => array(
                'Token' => WP_TEST_MENU_TOKEN,
              ),
            ); 
            //Get the currently set list item
            $request_uri = WP_TEST_MENU_API_URL.'/items/current';
            $request = wp_remote_get( $request_uri, $args  );
            
            if( is_wp_error( $request ) || '200' != wp_remote_retrieve_response_code( $request ) )
            return;
            
            $current_list_item_collection = json_decode( wp_remote_retrieve_body( $request ) );
            $current_list_item = $current_list_item_collection[0][0];

            //Get all the entries for list control
            $request_uri = WP_TEST_MENU_API_URL.'/items';
            $request = wp_remote_get( $request_uri, $args  );
            
            if( is_wp_error( $request ) || '200' != wp_remote_retrieve_response_code( $request ) )
              return;
            $list_data = json_decode( wp_remote_retrieve_body( $request ) );
    
            foreach($list_data as $list_item){?>
              <option value='<?php echo $list_item[0]?>' <?php echo ($list_item[0]==$current_list_item) ? 'selected' : ''?>><?php echo $list_item[1]?></option>
            <?php 
            }
            ?>
            </select>
            <p class="submit">
            <input type="submit" id="submit" class="button button-primary" value="Save">
            </p>
        </form>
      </div>
    </div>