<?php

/**
 * The admin-specific functionality of the plugin.
 * @package	WP_Test_Menu
 * @author	Sreejith C J	
 */
class WP_Test_Menu_Admin {

	/**
	 * The ID of this plugin.
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 * @param      string    $plugin_name  The name of this plugin.
	 * @param      string    $version       The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/** 
	 * Add WP Test Menu to admin menu
	*/
	function add_wp_test_menu() {
		add_menu_page('WP Test Menu Page', 'WP Test Menu', 'manage_options', 'wp-test-menu', array($this, 'html_form_page_content'), 'dashicons-menu-alt2');
	}

	/**
	 * Load markup for WP Test Menu page
	 */
	public function html_form_page_content() {
		include_once( 'partials/wp-test-menu-admin-display.php' );
	}
	
	/**
	 * Register the stylesheets for the admin area.
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-test-menu-admin.css', array(), $this->version, 'all' );

	}
	/**
	 * Register the js for the admin area.
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-test-menu-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Update the  selected list item as current item
	 */
	public function wp_test_menu_update_current_list_item(){

		$selected_list_item = $_POST['selected_item'];
		$data = array('id'=> $selected_list_item);
		$args = array(
			'headers'     => array(
							'Token' => WP_TEST_MENU_TOKEN,
							),
			'method' => 'PUT',
			'body' => json_encode($data),
		  ); 

		  $request_uri = WP_TEST_MENU_API_URL.'/items';
		  $request = wp_remote_request( $request_uri, $args  );
		 echo 'Successfully changed current list item';
		die();
	}
}
