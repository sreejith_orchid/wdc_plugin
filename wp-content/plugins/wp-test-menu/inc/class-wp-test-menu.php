<?php

/**
 * Core plugin class
 * @package	WP_Test_Menu
 * @author	Sreejith C J	
 */

class WP_Test_Menu {

	/**
	 * Unique identifier of this plugin.
	 * @var      string    $plugin_name
	 */
	protected $plugin_name;

	/**
	 * Current version of the plugin.
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Set the plugin name and the plugin version
	 */
	public function __construct() {
		if ( defined( 'WP_TEST_MENU_VERSION' ) ) {
			$this->version = WP_TEST_MENU_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'wp-test-menu';
	}

	/**
	 * Load the required dependencies for this plugin.
	 */
	private function load_dependencies() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wp-test-menu-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wp-test-menu-public.php';
	}

	/**
	 * Register all of the hooks related to the admin
	 */
	private function define_admin_hooks() {
		$plugin_admin = new WP_Test_Menu_Admin( $this->get_plugin_name(), $this->get_version() );
		add_action('admin_menu', array($plugin_admin, 'add_wp_test_menu'));
		add_action( 'admin_enqueue_scripts', array($plugin_admin, 'enqueue_styles' ));
		add_action( 'admin_enqueue_scripts', array($plugin_admin, 'enqueue_scripts' )); 
		add_action( 'wp_ajax_test_menu_form', array($plugin_admin, 'wp_test_menu_update_current_list_item'));

	}

	/**
	 * Register all of the hooks related to the public
	 */
	private function define_public_hooks() {

		$plugin_public = new WP_Test_Menu_Public( $this->get_plugin_name(), $this->get_version() );
		add_filter( 'the_content', array($plugin_public, 'prepend_to_content') );
		add_filter( 'get_the_excerpt', array($plugin_public, 'prepend_to_excerpt') );

	}
	 /**
	 * Execute all of the hooks
	 */
	public function run() {
		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();	
	} 

	/**
	 * The name of the plugin used to uniquely identify it. 
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * Retrieve the version number of the plugin.
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
