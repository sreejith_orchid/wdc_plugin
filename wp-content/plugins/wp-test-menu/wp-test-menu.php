<?php
/**
 * Plugin Name: WP Test Menu
 * Plugin URI: 
 * Description: Plugin to add list value to Blog
 * Version:     1.0.0
 * Author:      Sreejith C J
 * Author URI: http://www.sreejithcj.com/
 * License: GPLv2 or later
 * Text Domain: wp-test-menu
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Current plugin version.
 */
define( 'WP_TEST_MENU_VERSION', '1.0.0' );

/**
 * API Endpoint
 */
define('WP_TEST_MENU_API_URL', 'http://api.sreejithcj.com');

/**
 * API Token
 * Improvement - Will store this in cookie after JWT token implementation in backend application
 */
define('WP_TEST_MENU_TOKEN', '56dhwiwocncgftqrae#%cbc*2$%(khets#$n12x&56');

/**
 * Plugin activation.
 */
function activate_wp_test_menu() {
	require_once plugin_dir_path( __FILE__ ) . 'inc/class-wp-test-menu-activator.php';
	WP_Test_Menu_Activator::activate();
}

/**
 * Plugin deactivation.
 */
function deactivate_wp_test_menu() {
	require_once plugin_dir_path( __FILE__ ) . 'inc/class-wp-test-menu-deactivator.php';
	WP_Test_Menu_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_test_menu' );
register_deactivation_hook( __FILE__, 'deactivate_wp_test_menu' );

/**
 * Plugin class  for admin-specific hooks, and public site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'inc/class-wp-test-menu.php';

/**
 * Execution of the plugin.
 */
function run_wp_menu_test() {

	$wp_test_menu = new WP_Test_Menu();
	$wp_test_menu->run();

}
run_wp_menu_test();